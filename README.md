TVicPort to WinRing0 wrapper
============================

A C++ wrapper that maps methods from TVicPort.dll to the more secure WinRing0.dll driver.
Read more information about this in the Reddit thread: https://www.reddit.com/r/thinkpad/comments/49wtqw/hdd_led_for_all_thinkpads_hopefully/

After compiling, using VC++ 2015, put the generated TVicPort.dll and WinRing0 files alongside an app that uses the TVicPort driver, and when launched, the respective app will use WinRing0 instead of TVicPort. This way, TVicPort can be safely removed from the system. Please note that you have to launch the application as administrator in order for it to run properly, as WinRing0 requires all apps to be run as administrator in order to access its features.

Initial code and idea created by yet_another_usr
Published by ValiNet (Valentin-Gabriel Radu)