// TVicPort to WinRing0 wrapper
// Initial code and idea created by yet_another_usr
// Published by ValiNet (Valentin-Gabriel Radu)
//
// dllmain.c : Defines the entry point for the DLL application.
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif // !WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN

#if 0
#ifdef TVICPORT_EXPORTS
#define TVICPORT_API __declspec(dllexport)
#else
#define TVICPORT_API __declspec(dllimport)
#endif
#else
#define TVICPORT_API
#endif

// Windows Header Files:
#include <SDKDDKVer.h>
#include <windows.h>
#include "extlib\OlsApi.h"

#define VICFN WINAPI

static BOOL g_bHardAccess = TRUE;

#ifdef __cplusplus
extern "C" {
#endif

TVICPORT_API void VICFN CloseTVicPort()
{
	DeinitializeOls();
}

TVICPORT_API BOOL VICFN OpenTVicPort()
{
	return InitializeOls();
}

TVICPORT_API UCHAR VICFN ReadPort(USHORT PortAddr)
{
	return ReadIoPortByte(PortAddr);
}

TVICPORT_API VOID VICFN SetHardAccess(BOOL bNewValue)
{
	g_bHardAccess = bNewValue;
}

TVICPORT_API BOOL VICFN TestHardAccess()
{
	return g_bHardAccess;
}

TVICPORT_API VOID VICFN WritePort(USHORT PortAddr, UCHAR nNewValue)
{
	WriteIoPortByte(PortAddr, nNewValue);
}

TVICPORT_API BOOL VICFN IsDriverOpened()
{
	return !GetDllStatus();
}

#ifdef __cplusplus
}
#endif

BOOL WINAPI DllMain(HINSTANCE hModule, DWORD fdwReason, LPVOID lpReserved)
{
	UNREFERENCED_PARAMETER(lpReserved);
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		break;
	case DLL_PROCESS_DETACH:
		break;
	default:
		break;
	}

	return TRUE;
}
